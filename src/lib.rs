#![cfg_attr(not(test), no_std)]
///! `#![no_std]` Async Mutex
///! ========================
///!
///! Rust makes it possible to have mutexes even when you don't have threads!
///!
///! This mutex does not implement `Send` or `Sync` and is meant to be used with tokio's [spawn_local](https://docs.rs/tokio/1.6.0/tokio/task/fn.spawn_local.html) or something similar from an other executor.
mod mutex;
pub use mutex::{Mutex, MutexGuard};
