use core::cell::{Cell, UnsafeCell};
use core::future::Future;
use core::ops::{Deref, DerefMut};
use core::pin::Pin;
use core::ptr;
use core::task::{Context, Poll, Waker};

/// A Mutex to use for synchronization across asynchronous task in a single threaded executor
///
/// As such, it doesn't implement the Sync trait
/// The intended use case for this Mutex is to share it across task spawned locally
/// with tokio's [spawn_local](https://docs.rs/tokio/0.2.22/tokio/task/fn.spawn_local.html)
/// or something similar from an other executor
///
/// This Mutex works in no_std contexts
///
/// ```
/// # #[tokio::test]
/// # async fn basic_usage() {
/// use std::time::Duration;
/// use tokio::task;
/// use tokio::time;
///
/// let local = task::LocalSet::new();
/// let mu = Rc::new(Mutex::new(50u32));
/// let mu_cl = mu.clone();
/// let mu_cl2 = mu.clone();
/// local.spawn_local(async move {
///     let mut guard = mu_cl.lock().await;
///     assert_eq!(50, *guard);
///     *guard = 60;
///     assert_eq!(60, *guard);
///     time::timeout(Duration::from_millis(200), async {})
///         .await
///         .unwrap();
/// });
///
/// local.spawn_local(async move {
///     time::timeout(Duration::from_millis(100), async {})
///         .await
///         .unwrap();
///     let mut guard = mu_cl2.lock().await;
///     assert_eq!(60, *guard);
///     *guard = 70;
///     assert_eq!(70, *guard);
/// });
///
/// local.await;
/// assert_eq!(70, *mu.try_lock().unwrap());
/// # }
/// ```
///
/// Fairness
/// ========
///
/// Currently, fairness is not fully guaranted.
pub struct Mutex<T> {
    is_locked: Cell<bool>,
    data: UnsafeCell<T>,
    waiting_head: UnsafeCell<*const Waiting>,
}

impl<T> Mutex<T> {
    /// Creates a new Mutex
    pub fn new(data: T) -> Mutex<T> {
        Mutex {
            is_locked: Cell::new(false),
            data: UnsafeCell::new(data),
            waiting_head: UnsafeCell::new(ptr::null()),
        }
    }

    /// Try locking a Mutex, returning immediately with `Err`
    /// if the mutex is already locked
    ///
    /// Can be used outside of an async context
    pub fn try_lock(&self) -> Result<MutexGuard<'_, T>, ()> {
        if !self.is_locked.get() {
            self.is_locked.set(true);
            Ok(MutexGuard { mu: self })
        } else {
            Err(())
        }
    }

    /// Obtain a WaitingGuard, which can then be polled to obtain a lock on the Mutex
    fn acquire(&self) -> WaitingGuard<'_, T> {
        WaitingGuard {
            mu: self,
            waiting: Waiting {
                waker: None,
                next: UnsafeCell::new(ptr::null()),
                prev: UnsafeCell::new(ptr::null()),
            },
        }
    }

    /// Obtain a lock on the Mutex
    pub async fn lock(&self) -> MutexGuard<'_, T> {
        self.acquire().await
    }

    /// Try to get a lock on the Mutex
    /// If the Mutex is already locked, returns `Pending` and registers the waker to be waked once
    /// the Mutex is free
    fn poll_lock(&self) -> Poll<MutexGuard<'_, T>> {
        match self.try_lock() {
            Ok(guard) => Poll::Ready(guard),
            Err(()) => Poll::Pending,
        }
    }

    /// Drop the lock
    unsafe fn unlock(&self) {
        assert!(self.is_locked.replace(false));
        let waiting_head = *self.waiting_head.get();
        if !waiting_head.is_null() {
            if let Some(w) = &(*waiting_head).waker {
                w.wake_by_ref();
            }
        }
    }
}

/// The doubly linked list of wakers
struct Waiting {
    waker: Option<Waker>,
    next: UnsafeCell<*const Waiting>,
    prev: UnsafeCell<*const Waiting>,
}

/// Future waiting that is `Ready` once the lock is acquired
struct WaitingGuard<'m, T> {
    mu: &'m Mutex<T>,
    waiting: Waiting,
}

impl<'m, T> WaitingGuard<'m, T> {
    unsafe fn pointer_drop(&mut self) {
        if (*self.waiting.prev.get()).is_null() {
            *self.mu.waiting_head.get() = *self.waiting.next.get();
        } else {
            *(**self.waiting.prev.get()).next.get() = *self.waiting.next.get();
        }

        if !(*self.waiting.next.get()).is_null() {
            *(**self.waiting.next.get()).next.get() = *self.waiting.prev.get();
        }
    }
}

impl<'m, T> Future for WaitingGuard<'m, T> {
    type Output = MutexGuard<'m, T>;

    fn poll(self: Pin<&mut Self>, cx: &mut Context) -> Poll<Self::Output> {
        let s = self.get_mut();
        if let Poll::Ready(guard) = s.mu.poll_lock() {
            s.waiting.waker = None;
            unsafe { s.pointer_drop() };
            return Poll::Ready(guard);
        }

        if s.waiting.waker.is_none() {
            s.waiting.waker = Some(cx.waker().clone())
        }

        return Poll::Pending;
    }
}

impl<'m, T> Drop for WaitingGuard<'m, T> {
    fn drop(&mut self) {
        // No waker means that either:
        //  - Poll hasn't been called, so the mutex isn't aware of the waiting
        //  - The WaitingGuard has been polled and returned ready, and did the pointer manipulation
        //  then
        if self.waiting.waker.is_some() {
            unsafe { self.pointer_drop() };
        }
    }
}

impl<'m, T> Drop for MutexGuard<'m, T> {
    fn drop(&mut self) {
        unsafe { self.mu.unlock() }
    }
}

/// Handle to a held `Mutex`
pub struct MutexGuard<'m, T> {
    mu: &'m Mutex<T>,
}

impl<'m, T> Deref for MutexGuard<'m, T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        unsafe { &*self.mu.data.get() }
    }
}

impl<'m, T> DerefMut for MutexGuard<'m, T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { &mut *self.mu.data.get() }
    }
}

#[cfg(test)]
mod tests {
    use super::*;
    use std::mem::drop;
    use std::rc::Rc;

    #[tokio::test]
    async fn simple() {
        let mu = Mutex::new(50u32);
        let mut guard = mu.lock().await;
        assert_eq!(50, *guard);
        *guard = 60;
        assert_eq!(60, *guard);

        drop(guard);

        let guard_bis = mu.lock().await;
        assert_eq!(60, *guard_bis);
    }
    #[tokio::test]
    async fn multitask() {
        use std::time::Duration;
        use tokio::task;
        use tokio::time;

        let local = task::LocalSet::new();
        let mu = Rc::new(Mutex::new(50u32));
        let mu_cl = mu.clone();
        let mu_cl2 = mu.clone();
        local.spawn_local(async move {
            let mut guard = mu_cl.lock().await;
            assert_eq!(50, *guard);
            *guard = 60;
            assert_eq!(60, *guard);
            time::timeout(Duration::from_millis(200), async {})
                .await
                .unwrap();
        });

        local.spawn_local(async move {
            time::timeout(Duration::from_millis(100), async {})
                .await
                .unwrap();
            let mut guard = mu_cl2.lock().await;
            assert_eq!(60, *guard);
            *guard = 70;
            assert_eq!(70, *guard);
        });

        local.await;
        assert_eq!(70, *mu.try_lock().unwrap());
    }
}
